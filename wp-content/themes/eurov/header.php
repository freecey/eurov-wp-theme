<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="language" content="FR" />
    <meta name="geo.region" content="BE-WLG" />
    <meta name="geo.placename" content="Verviers, Liège" />
    <meta name="geo.position" content="50.5895954;5.8640882" />
    <meta name="ICBM" content="50.5895954, 5.8640882" />
    <link rel="icon" type="images/jpg" href="<?= get_template_directory_uri(); ?>/assets/images/favicon.jpg" />
    <link rel="shortcut icon" type="images/x-icon" href="<?= get_template_directory_uri(); ?>/assets/images/favicon.ico" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">
    <?php wp_head(
        [
            'container' => false,
            'menu_class' => 'liste-nav',
            '',
        ]
    ); ?>
    <style>
        <?php if( get_field('main_bg_color', 24) ): ?>
        .logo-cont {
            background-color: <?= esc_html(get_field('main_bg_color', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('navbar_bg_color', 24) ): ?>
        .navbar-top {
            background-color: <?= esc_html(get_field('navbar_bg_color', 24)); ?>; }
        <?php endif; ?>
        <?php if( get_field('navbar_text', 24) ): ?>
        .navbar-light .navbar-nav .nav-link {
            color: <?= esc_html(get_field('navbar_text', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('navbar_bg_hover', 24) || get_field('navbar_text_hover', 24)): ?>
        .navbar-top li.menu-item :hover {
        <?php if( get_field('navbar_bg_hover', 24) ): ?>
            background-color: <?= esc_html(get_field('navbar_bg_hover', 24)); ?> !important;
        <?php endif; ?>
        <?php if( get_field('navbar_text_hover', 24) ): ?>
            color: <?= esc_html(get_field('navbar_text_hover', 24)); ?> !important;
        <?php endif; ?>
        }
        <?php endif; ?>
        <?php if( get_field('navbar_text_active', 24) || get_field('main_bg_color', 24) ): ?>
        .navbar-top li.active > a {
        <?php if( get_field('navbar_text_active', 24) ): ?>
            color: <?= esc_html(get_field('navbar_text_active', 24)); ?> !important;
        <?php endif; ?>
        <?php if( get_field('main_bg_color', 24) ): ?>
            background-color: <?= esc_html(get_field('main_bg_color', 24)); ?> !important;
        <?php endif; ?>
        }
        <?php endif; ?>
        <?php if( get_field('main_bg_color', 24) || get_field('main_color', 24)): ?>
        #main-cont {
        <?php if( get_field('main_bg_color', 24) ): ?>
            background-color: <?= esc_html(get_field('main_bg_color', 24)); ?> !important;
        <?php endif; ?>
        <?php if( get_field('main_color', 24) ): ?>
            color: <?= esc_html(get_field('main_color', 24)); ?> !important;
        <?php endif; ?>
        }
        <?php endif; ?>
        <?php if( get_field('submenu_bg', 24) ): ?>
        .submenu {
            background-color: <?= esc_html(get_field('submenu_bg', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('submenu_text', 24) ): ?>
        .submenu li a {
            color: <?= esc_html(get_field('submenu_text', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('submenu_bg_active', 24) ): ?>
        .submenu-active {
            background-color: <?= esc_html(get_field('submenu_bg_active', 24)); ?> !important;
        }
        <?php endif; ?>
        <?php if( get_field('submenu_text_active', 24) ): ?>
        .submenu-active a {
            color: <?= esc_html(get_field('submenu_text_active', 24)); ?> !important;
        }
        <?php endif; ?>
        <?php if( get_field('submenu_bg_hover', 24) ): ?>
        .submenu li:hover {
            background-color: <?= esc_html(get_field('submenu_bg_hover', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('submenu_text_hover', 24) ): ?>
        .submenu li:hover a {
            color: <?= esc_html(get_field('submenu_text_hover', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('portf_bg', 24) ): ?>
        .menu-portf {
            background-color: <?= esc_html(get_field('portf_bg', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('portf_text', 24) ): ?>
        .menu-portf li a {
            color: <?= esc_html(get_field('portf_text', 24)); ?>;
        }
        <?php endif; ?>
        <?php if( get_field('portf_bg_active', 24) ): ?>
        .menu-portf .active {
            background-color: <?= esc_html(get_field('portf_bg_active', 24)); ?> !important;
        }
        <?php endif; ?>
        <?php if( get_field('portf_text_active', 24) ): ?>
        .menu-portf .active a {
            color: <?= esc_html(get_field('portf_text_active', 24)); ?> !important;
        }
        <?php endif; ?>
        <?php if( get_field('portf_bg_hover', 24) ): ?>
        .menu-portf li:hover {
            background-color: <?= esc_html(get_field('portf_bg_hover', 24)); ?> !important;
        }
        <?php endif; ?>
        <?php if( get_field('portf_text_hover', 24) ): ?>
        .menu-portf li:hover a {
            color: <?= esc_html(get_field('portf_text_hover', 24)); ?> !important;
        }
        <?php endif; ?>
        <?php if( get_field('footer_bg', 24) || get_field('footer_text', 24) ): ?>
        .footer-bg {
        <?php if( get_field('footer_bg', 24) ): ?>
            background-color: <?= esc_html(get_field('footer_bg', 24)); ?>;
        <?php endif; ?>
        <?php if( get_field('footer_text', 24) ): ?>
            color: <?= esc_html(get_field('footer_text', 24)); ?>;
        <?php endif; ?>
        }
        <?php endif; ?>
        <?php if( get_field('footer_text', 24) ): ?>
        footer a {
            color: <?= esc_html(get_field('footer_text', 24)); ?>;
        }
        <?php endif; ?>
    </style>
</head>

<body class="d-flex flex-column vh-100">
    <main class="flex-shrink-0" id="main-cont">
        <?php get_template_part('template-parts/header/_navbar'); ?>