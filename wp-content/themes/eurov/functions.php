<?php

/**
 * Functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage EuroV
 * @since EuroV 1.0
 */


// Custom Post definitions
require_once(get_stylesheet_directory() . '/inc/post-custom.php');
// Custom Post Portfolio definitions
require_once(get_stylesheet_directory() . '/inc/post-portfolio.php');
// Custom Post Matieres
require_once(get_stylesheet_directory() . '/inc/post-matieres.php');
// Custom Post Guide des Tailles
require_once(get_stylesheet_directory() . '/inc/post-tailles.php');
// Custom shortcode definitions
require_once(get_stylesheet_directory() . '/inc/shortcode.php');
// Custom navbar definitions
require_once(get_stylesheet_directory() . '/inc/navbar.php');
// Image upscale resize
require_once(get_stylesheet_directory() . '/inc/img-resize.php');
// Custom Functions
require_once(get_stylesheet_directory() . '/inc/new-funct.php');
// Rewrite URL for portfolio add terms in URL
require_once(get_stylesheet_directory() . '/inc/rewrite-url-portfolio.php');

function eurov_supports()
{
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_theme_support('menus');
    add_theme_support('html5', ['script', 'style']);
    register_nav_menu('header', 'TOP NAVBAR');
    register_nav_menu('footer', 'FOOTER NAVBAR');
    add_image_size('carousel', 526, 333, true);
    add_image_size('carousel-p', 525, 550, false);
    add_image_size('carousel-p2', 516, 550, false);
}

function eurov_assets()
{
    wp_register_style('EuroV_Style', get_template_directory_uri() . '/assets/css/style.css');
    wp_enqueue_style('normalize');
    wp_enqueue_style('EuroV_Style');
    wp_enqueue_script('BS_js', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array('jquery'), false, true);
    wp_enqueue_script("jquery");
    wp_enqueue_script('JS_Navbar', get_template_directory_uri() . '/assets/js/navbar.js', array('BS_js'), 1.0, true);
    wp_enqueue_script('BS_FAS', get_template_directory_uri() . '/assets/js/solid.min.js', array(), '5.15.3', true);
    // if (is_single('contact')) {
    //     wp_enqueue_script('Form_JS', get_template_directory_uri() . '/assets/js/form.js', array('jquery'), false, true);
    // }
}

function my_remove_admin_menus()
{
    remove_menu_page('edit-comments.php');
}

add_action('after_setup_theme', 'eurov_supports');
add_action('wp_enqueue_scripts', 'eurov_assets');
add_action('admin_init', 'my_remove_admin_menus');


function wpdocs_remove_plugin_image_sizes()
{
    remove_image_size('2048x2048');
    remove_image_size('1536x1536');
    remove_image_size('large');
    remove_image_size('medium_large');
}
add_action('init', 'wpdocs_remove_plugin_image_sizes');



/* Register template redirect action callback */
add_action('template_redirect', 'meks_remove_wp_archives');

/* Remove archives */
function meks_remove_wp_archives()
{
    //If we are on category or tag or date or author archive
    if (is_category() || is_tag() || is_date() || is_author()) {
        global $wp_query;
        $wp_query->set_404(); //set to 404 not found page
    }
}

/* Exclude Multiple Taxonomies From Yoast SEO Sitemap */
add_filter('wpseo_sitemap_exclude_taxonomy', 'sitemap_exclude_taxonomy', 10, 2);
function sitemap_exclude_taxonomy($value, $taxonomy)
{
    $taxonomy_to_exclude = array('category-Professionnels', 'category-maison', 'category-matieres');
    if (in_array($taxonomy, $taxonomy_to_exclude)) return true;
}

function post_remove()
{
    remove_menu_page('edit.php');
}
add_action('admin_menu', 'post_remove');
