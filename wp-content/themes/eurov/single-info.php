<?php get_header(); ?>


<div class="container my-4">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-10">
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="col3-middle">
                            <?php get_template_part('template-parts/content/_col2-content'); ?>
                        </div>

                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">

                        <?php
                        if (get_field('enable_carousel') == 1) :
                            get_template_part('template-parts/content/_carousel');
                        elseif (get_field('enable-galleries-list') == 1) :
                            get_template_part('template-parts/content/_galleries-list');
                        elseif (get_field('enable_map') == 1) :
                            get_template_part('template-parts/content/_gmaps');
                        endif;
                        ?>
                        <?php get_template_part('template-parts/content/_contact-add-map'); ?>
                        <?php get_template_part('template-parts/content/_col3-content'); ?>

                    </div>


                    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <?php get_template_part('template-parts/content/_subcontent'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>