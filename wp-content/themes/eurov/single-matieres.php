<?php get_header(); ?>


<div class="container my-4">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-10">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 mb-4">
                        <div class="col3-middle">
                            <h2 class="text-center my-2"><?= get_the_title(); ?></h2>
                        </div>
                        <?php get_template_part('template-parts/header/_navguide'); ?>
                        <div class="col3-middle">
                            <?php get_template_part('template-parts/content/_col2-content'); ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">

                        <?php
                        if (get_field('enable_carousel') == 1) :
                            get_template_part('template-parts/content/_carousel');
                        endif;
                        ?>
                        <?php get_template_part('template-parts/content/_col3-content'); ?>

                    </div>
                </div>
            </div>
        </section>
        <div class="col-12 col-md-12 col-lg-2  my-auto">
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-10">
            <?php get_template_part('template-parts/content/_subcontent'); ?>
        </div>

    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>