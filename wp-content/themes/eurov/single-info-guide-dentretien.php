<?php get_header(); ?>


<div class="container my-4">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-10">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="col3-middle">
                            <?php get_template_part('template-parts/content/_col2-content'); ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <?php
                        // check for rows (parent repeater)
                        if (have_rows('rep_section')) :
                            $col2_nmb_row = get_field('nmb_sec_col2');
                            // echo $col2_nmb_row;
                            $col2_count = 1;
                        ?>
                            <div id="rep_section">
                                <?php

                                // loop through rows (parent repeater)
                                while (have_rows('rep_section')) : the_row();
                                    if ($col2_count <= $col2_nmb_row) :
                                ?>
                                        <div>
                                            <h5><?php the_sub_field('titre_section'); ?></h5>
                                            <?php

                                            // check for rows (sub repeater)
                                            if (have_rows('rep_instruction')) : ?>
                                                <ul class="sec_entretien">
                                                    <?php

                                                    // loop through rows (sub repeater)
                                                    while (have_rows('rep_instruction')) : the_row();

                                                        // display each item as a list - with a class of completed ( if completed )
                                                    ?>
                                                        <li><img src="<?= get_sub_field('instruction_logo')['url']; ?>" alt="<?php the_sub_field('instruction_text'); ?>"><span class="ps-4"><?php the_sub_field('instruction_text'); ?></span></li>
                                                    <?php endwhile; ?>
                                                </ul>
                                            <?php endif; //if( get_sub_field('rep_instruction') ): 
                                            ?>
                                        </div>

                                <?php $col2_count++;
                                    endif;
                                endwhile; // while( has_sub_field('rep_section') ): 
                                ?>
                            </div>
                        <?php endif; // if( get_field('rep_section') ): 
                        ?>



                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">

                        <?php
                        // check for rows (parent repeater)
                        if (have_rows('rep_section')) :
                            $col2_nmb_row = get_field('nmb_sec_col2');
                            // echo $col2_nmb_row;
                            $col2_count = 1;
                        ?>
                            <div id="rep_section">
                                <?php

                                // loop through rows (parent repeater)
                                while (have_rows('rep_section')) : the_row();
                                    if ($col2_count > $col2_nmb_row) :
                                ?>
                                        <div>
                                            <h5><?php the_sub_field('titre_section'); ?></h5>
                                            <?php

                                            // check for rows (sub repeater)
                                            if (have_rows('rep_instruction')) : ?>
                                                <ul class="sec_entretien">
                                                    <?php

                                                    // loop through rows (sub repeater)
                                                    while (have_rows('rep_instruction')) : the_row();

                                                        // display each item as a list - with a class of completed ( if completed )
                                                    ?>
                                                        <li><img src="<?= get_sub_field('instruction_logo')['url']; ?>" alt="<?php the_sub_field('instruction_text'); ?>"><span class="ps-4"><?php the_sub_field('instruction_text'); ?></span></li>
                                                    <?php endwhile; ?>
                                                </ul>
                                            <?php endif; //if( get_sub_field('rep_instruction') ): 
                                            ?>
                                        </div>

                                <?php $col2_count++;
                                    else : $col2_count++;
                                    endif;
                                endwhile; // while( has_sub_field('rep_section') ): 
                                ?>
                            </div>
                        <?php endif; // if( get_field('rep_section') ): 
                        ?>

                        <?php get_template_part('template-parts/content/_col3-content'); ?>

                    </div>
                </div>
            </div>
        </section>
        <div class="col-12 col-md-12 col-lg-2  my-auto">
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-10">
            <?php get_template_part('template-parts/content/_subcontent'); ?>
        </div>

    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>