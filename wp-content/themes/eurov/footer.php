</main>
<footer class="footer mt-auto footer-bg text-center text-lg-start">

    <div class="container p-4">

        <div class="row">

            <div class="col-lg-5 col-md-12 mb-4 mb-md-0">
                <h5 class="text-uppercase">
                    <a href="/"><img src="<?= get_field('logo', 7)['url']; ?>" alt="Vrancken Logo" style="width: 350px; height:auto;"></a>
                </h5>
                <p class="mt-4">
                    <?= esc_html(get_field('descriptionslogan', 7)); ?>
                </p>
                <?php if (have_rows('horraires', 7)) : ?>
                    <ul class="list-unstyled">
                        <?php while (have_rows('horraires', 7)) : the_row(); ?>
                            <li>
                                <i class="fas fa-clock"></i>
                                <span class="ms-2">
                                    <?= esc_html(get_sub_field('horraire')); ?>
                                </span>
                            </li>
                            <li></li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>

            <div class="col-lg-4 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase">CONTACT</h5>

                <ul class="list-unstyled mb-0">
                    <li><i class="fas fa-home"></i>
                        <span class="ms-2">
                            <?= esc_html(get_field('address', 7)); ?>
                        </span>
                    </li>
                    <li>
                        <i class="fas fa-phone"></i>
                        <span class="ms-2">
                            <a href="tel:<?= esc_html(get_field('phone', 7)); ?>"><?= esc_html(get_field('phone', 7)); ?></a>
                    </li>
                    <li><i class="far fa-envelope"></i>
                        <span class="ms-2">
                            <a href="mailto:<?= esc_html(get_field('email_eurovranckencom', 7)); ?>"><?= esc_html(get_field('email_eurovranckencom', 7)); ?></a>
                        </span>
                    </li>
                    <li><i class="fas fa-globe"></i>
                        <span class="ms-2">
                            <a href="<?= esc_html(get_field('site_eurovranckencom', 7)['url']); ?>"><?= esc_html(get_field('site_eurovranckencom', 7)['title']); ?></a>
                        </span>
                    </li>
                    <li><i class="far fa-envelope"></i>
                        <span class="ms-2">
                            <a href="mailto:<?= esc_html(get_field('email_vranckenshopcom', 7)); ?>"><?= esc_html(get_field('email_vranckenshopcom', 7)); ?></a>
                        </span>
                    </li>
                    <li><i class="fas fa-globe"></i>
                        <span class="ms-2">
                            <a href="<?= esc_url(get_field('site_vranckenshopcom', 7)['url']); ?>"><?= esc_html(get_field('site_vranckenshopcom', 7)['title']); ?></a>
                        </span>
                    </li>
                </ul>
            </div>

            <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
                <h5 class="text-uppercase mb-2">Info</h5>
                <?php if (have_rows('liens', 7)) : ?>
                    <ul class="list-unstyled">
                        <?php while (have_rows('liens', 7)) : the_row();
                            $link = get_sub_field('url'); ?>
                            <li><a href="<?= esc_url($link['url']); ?>" target="<?= esc_attr($link['target'] ? $link['target'] : '_self'); ?>"><?= esc_html($link['title']); ?></a></li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        <div class="footer-copy">
            © Copyright 2021 | Ets VRANCKEN | Tous droits réservés | Réalisé par
            <a href="https://www.esi-informatique.com/">ESI Informatique</a>
        </div>
    </div>
    <?php wp_footer() ?>
</footer>
</body>

</html>