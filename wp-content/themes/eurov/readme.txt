=== EuroV ===
Contributors: audritdotbe
Requires at least: 5.7
Tested up to: 5.7
Requires PHP: 7.4
Stable tag: 1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Wordpress for EuroVrancken - portail shop


== Changelog ==

= 1.0 =
* Released: June 3, 2021
