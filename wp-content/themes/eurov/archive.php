<?php get_header(); ?>

<div class="container my-4">
    <?php
    get_template_part('part/_breadcrumb')
    ?>
    <div class="row">
        <div class="col-12 col-md-12 col-lg-2">


            <?php
            $page = get_page_by_path('maison');
            echo get_the_ID($page);

            $post_objects = get_field('submenu');
            var_dump($post_objects);
            if ($post_objects) : ?>
                <ul>
                    <?php foreach ($post_objects as $post_object) : ?>
                        <li>
                            <a href="<?php echo get_permalink($post_object["ID"]); ?>"><?php echo get_the_title($post_object["ID"]); ?></a>
                            <?= $post_object["ID"]; ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif;

            ?>


        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-5">
            <div id="centre">
                <h1>Vrancken</h1>

                <h2>Les Etablissements <span class="blue">VRANCKEN</span> <br>
                    vous accueillent depuis 1903.</h2>

                <p>Vrancken, c'est plus d'un siècle de <br> choix, de qualité, de prix et de service<br>
                    dans le <strong>linge de maison</strong> et <br>
                    le <strong>vêtement de travail professionnel</strong>.</p>

                <p>Notre établissement de "Gros &amp; Détail" est accessible<br>
                    tant aux particuliers qu'aux professionnels.</p>

                <h2>Nos produits et services</h2>

                <p>Vrancken vous propose de <strong>nombreux articles</strong> en<br> linges de maison et vêtements professionnels.</p>
                <p>Nous <strong>confectionnons</strong> également à façon et sur mesure<br> dans nos ateliers.</p>
                <p>Nous <strong>personnalisons</strong> votre linge par des broderies.</p>
                <p>Nous acceptons volontiers vos <strong>listes de cadeaux</strong> (naissance, anniversaire, mariage, etc.)</p>

                <!--<h2><strong>Les Etablissements <span class="blue">VRANCKEN</span> <br/>
      		vous accueillent depuis 1903.</strong></h2
            <p><span class="blue">GROS & DETAIL</span></p><p>
        </p>
        <p>
            <strong>
                <span class="gblue">LINGE DE MAISON</span> <br />
                & <br />
                <span class="gblue">VÊTEMENTS DE TRAVAIL</span>
            </strong>
        </p>-->
            </div>

        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-5">
            <div id="centre2">
                <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="http://eurovrancken.com/images/index/vitrine_printemps_2014.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="http://eurovrancken.com/images/index/vitrine_vrancken_printemps_ete_2.jpg" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="http://eurovrancken.com/images/index/vitrine_mars_2013.jpg" class="d-block w-100" alt="...">
                        </div>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>



                <p><strong>Magasin ouvert du lundi au vendredi de<br>
                        9H30 à 12H et de 14H à 18H.<br>
                        Samedi de 9H30 à 12H.</strong><br>
                    <strong><span class="blue">Rue du Centre 43<br>
                            4800 Verviers, Belgique</span></strong><br>
                    <strong><span class="blue">Tél: (0032) 087 222813<br>
                            Fax: (0032) 087 226101</span><br></strong>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>