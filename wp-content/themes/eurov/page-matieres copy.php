<!-- HEAD.php -->
<?php get_header(); ?>



<div class="container my-4">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-10">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="col3-middle">
                            <?php
                            $portfolie_type = $post->post_type;
                            if ($portfolie_type == 'portfolio-maison') {
                                $portfolie_tax = 'category-maison';
                                $terms_array = get_the_terms($post->ID, $portfolie_tax);
                                $portfolie_term_object = $terms_array[0];
                                $col2_title = get_object_vars($portfolie_term_object)["name"];
                            } elseif ($portfolie_type == 'portfolio-prof') {
                                $portfolie_tax = 'category-Professionnels';
                                $terms_array = get_the_terms($post->ID, $portfolie_tax);
                                $portfolie_term_object = $terms_array[0];
                                $col2_title = get_object_vars($portfolie_term_object)["name"];
                            } else {
                                $col2_title = get_the_title();
                            }; ?>

                            <h2 class="text-center my-2"><?= $col2_title; ?></h2>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="col3-middle">
                            <?php get_template_part('template-parts/header/_navportfolio'); ?>

                            <?php get_template_part('template-parts/content/_col2-content'); ?>
                        </div>
                    </div>


                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">

                        <?php
                        if (get_field('enable_carousel') == 1) :
                            get_template_part('template-parts/content/_carousel');
                        endif;
                        ?>
                        <?php get_template_part('template-parts/content/_col3-content'); ?>

                    </div>
                </div>
            </div>
    </div>
    </section>
    <div class="col-12 col-md-12 col-lg-2  my-auto">
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-10">
        <?php get_template_part('template-parts/content/_subcontent'); ?>
    </div>

</div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>