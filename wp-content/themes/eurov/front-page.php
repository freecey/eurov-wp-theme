<?php get_header(); ?>



<div class="container my-4">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>

            <div class="col-12 col-sm-12 col-md-6 col-lg-5">
                <div class="col-middle">
                    <?php get_template_part('template-parts/content/_col2-content'); ?>
                </div>

            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-5">

                <?php
                if (get_field('enable_carousel') == 1) :
                    get_template_part('template-parts/content/_carousel');
                endif;
                ?>
                <?php get_template_part('template-parts/content/_col3-content'); ?>

            </div>
        </section>

        <div class="col-12 col-md-12 col-lg-2  my-auto">
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-10">
            <?php get_template_part('template-parts/content/_subcontent'); ?>
        </div>

    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>