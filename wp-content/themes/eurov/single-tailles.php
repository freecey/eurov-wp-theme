<?php get_header(); ?>


<div class="container my-4">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-10">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="col3-middle">
                            <h2 class="text-center my-2 mb-4"><?= get_the_title(); ?></h2>
                            <?php get_template_part('template-parts/header/_navguide'); ?>
                            <?php $contenu_into = get_field('contenu_into');
                            if ($contenu_into) : ?>
                                <div>
                                    <?= $contenu_into; ?>
                                </div>
                            <?php endif;
                            ?>
                        </div>

                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6">

                    </div>

                </div>
                <?php if (have_rows('rep_section')) :
                    $repeater = get_field('rep_section');
                    $order = array();
                    // populate order
                    // $reversed = array_reverse($repeater);
                    // var_dump($reversed);
                    foreach ($repeater as $i => $row) {
                        $order[$i] = $row['id'];
                    }
                    // array_multisort($order, SORT_DESC, $reversed);
                    if ($repeater) :
                        foreach ($repeater as $i => $row) : ?>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 my-3 order-md-1 order-sm-2">
                                    <div class="col3-middle">
                                        <?= $row['contenu-col2']; ?>
                                    </div>

                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 my-3 text-center order-md-2 order-sm-1">

                                    <img src="<?= $row['image']["url"]; ?>" alt="<?= $row['image']["name"]; ?>" style="max-width:100%; max-height: 250px;">

                                </div>
                            </div>
                <?php endforeach;
                    endif;
                endif; ?>

            </div>

            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <?php get_template_part('template-parts/content/_subcontent'); ?>
            </div>
        </section>
    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>