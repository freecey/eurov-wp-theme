<!-- HEAD.php -->
<?php get_header(); ?>



<div class="container my-5">
    <div class="row">
        <section class="row mb-4">
            <div class="col-12 col-md-12 col-lg-2">

                <?php get_template_part('template-parts/content/_submenu'); ?>

            </div>

            <div class="col-12 col-sm-12 col-md-10 col-lg-10">

                <h3 class="text-center mt-5">La page demandée n'existe pas</h3>
                <p class="text-center">(404 - page non trouvée)</p>
                <p class="text-center mt-5"><a class="text-center" href="/">Retourner a la page d'accueil</a></p>

            </div>
        </section>

    </div>
</div>
<!-- FOOTER.php -->
<?php get_footer(); ?>