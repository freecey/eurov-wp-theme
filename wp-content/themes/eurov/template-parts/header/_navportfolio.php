<?php
if (get_field('portfolioID')) {
    $portfolioOB = get_object_vars(get_field('portfolioID'));
    $portfolie_type_array = get_post_types_by_cust_taxonomy($portfolioOB["taxonomy"]);
    $portfolie_type = $portfolie_type_array[0];
    $portfolie_tax = $portfolioOB["taxonomy"];
    $portfolie_term_id = $portfolioOB["term_id"];
} else {
    $portfolie_type = $post->post_type;
    if ($portfolie_type == 'portfolio-maison') {
        $portfolie_tax = 'category-maison';
    } elseif ($portfolie_type == 'portfolio-prof') {
        $portfolie_tax = 'category-Professionnels';
    }
    $terms_array = get_the_terms($post->ID, $portfolie_tax);
    $portfolie_term_id_object = $terms_array[0];
    $portfolie_term_id = get_object_vars($portfolie_term_id_object)["term_id"];
}
$current_id = get_the_ID();
// LIST PORTF START

$args = array(
    'post_type' => $portfolie_type,
    'showposts' => -1,
    'orderby' => 'date',
    'order' => 'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => $portfolie_tax,
            'terms' => array($portfolie_term_id)
        )
    )
);
$loop = new WP_Query($args); ?>
<ul class="menu-portf my-4">
    <?php while ($loop->have_posts()) : $loop->the_post(); ?>

        <?php if (get_the_ID() == $current_id) {
            $add_class = ' active';
        } else {
            $add_class = '';
        };
        the_title('<li class="portf-title py-1 text-center' . $add_class . '"><a href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '">', '</a></li>'); ?>

    <?php endwhile; ?>
</ul>
<?php wp_reset_postdata();  ?>