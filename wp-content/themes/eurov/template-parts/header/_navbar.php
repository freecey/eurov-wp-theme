<div class="navbar-cont navbar sticky-top" id="navbar-cont">
    <div class="container-fluid text-center d-lg-block d-md-none d-none logo-cont">
        <img src="/wp-content/themes/eurov/assets/images/Logo-Ets-Vrancken-1903.png" alt="Logo Vrancken" height="100">
    </div>
    <nav class="navbar navbar-expand-lg navbar-light navbar-mainbg navbar-top w-100">
        <div class="container-fluid">
            <a class="navbar-brand navbar-logo d-block d-lg-none d-md-block" id="nav-logo" href="/"><img src="/wp-content/themes/eurov/assets/images/Logo-Ets-Vrancken-1903.png" alt="Logo Vrancken"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0  mx-auto">
                    <?php
                    wp_nav_menu(array(
                        'container' => false, // Removes the container, leaving just the ul element
                        'items_wrap' => '%3$s',
                        'add_li_class'  => 'nav-item',
                        'link_class' => 'nav-link'
                    ));
                    ?>
                </ul>
                <div class="menu-lang">
                    <?php echo do_shortcode('[gtranslate]'); ?>
                </div>
            </div>
        </div>
    </nav>
</div>
<script>
    // NAVBAR adaption for custom post & page
    var currentLocation = window.location.pathname;
    const menu1 = document.getElementById("menu-item-93");
    const menu2 = document.getElementById("menu-item-94");
    const menu3 = document.getElementById("menu-item-170");
    // console.log(currentLocation);

    if (currentLocation.includes("maison")) {
        menu2.classList.add("active");
    } else if (currentLocation.includes("prof")) {
        menu3.classList.add("active");
    } else {
        menu1.classList.add("active");
    }
</script>