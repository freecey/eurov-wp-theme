<?php
$slug = get_post_field('post_name', get_post());
$current_id = get_the_ID();
// echo $slug;
$post_type = get_post_type();
// var_dump($slug);
// var_dump($post_type);
if ($post_type != 'page') {
    $post_type_data = get_post_type_object($post_type);
    $post_type_slug = $post_type_data->rewrite['slug'];
    // var_dump($post_type_slug);
    if ($post_type_slug == 'portfolio-maison/%cat%') {
        $post_type_slug = 'linge-de-maison';
        $taxonomy_names = get_post_taxonomies();
        $term_list = wp_get_post_terms($post->ID, $taxonomy_names[0], array('fields' => 'all'));
        $menu_post = get_page_by_path(get_object_vars($term_list[0])["slug"], OBJECT, 'maison');
        $current_id = $menu_post->ID;
    } elseif ($post_type_slug == 'portfolio-professionnels/%cat%') {
        $post_type_slug = 'vetements-professionnels';
        $taxonomy_names = get_post_taxonomies();
        $term_list = wp_get_post_terms($post->ID, $taxonomy_names[0], array('fields' => 'all'));
        $menu_post = get_page_by_path(get_object_vars($term_list[0])["slug"], OBJECT, 'professionnels');
        $current_id = $menu_post->ID;
    } elseif ($post_type_slug == 'info') {
        $post_type_slug = 'accueil';
    } elseif ($post_type_slug == 'matieres') {
        $post_type_slug = 'accueil';
    } elseif ($post_type_slug == 'tailles') {
        $post_type_slug = 'accueil';
    }

    $page_id = get_id_by_slug($post_type_slug);
}

$post_objects = get_field('submenu', $page_id);

if ($post_objects == NULL) {
    $post_type_slug = 'accueil';
    $page_id = get_id_by_slug($post_type_slug);
    $post_objects = get_field('submenu', $page_id);
}

if ($slug == 'tailles' or $slug == 'matieres') {
    $post_type_slug = 'accueil';
    $page_id = get_id_by_slug($post_type_slug);
    $post_objects = get_field('submenu', $page_id);
}

// var_dump($post_objects);
if ($post_objects) : ?>
    <div class="submenu">
        <ul class="list-unstyled text-left d-flex d-md-flex justify-content-around flex-wrap flex-md-wrap flex-lg-nowrap d-lg-block">
            <?= ''; //(isset($add_to_menu)) ? $add_to_menu : ''; 
            ?>
            <?php foreach ($post_objects as $post_object) : ?>
                <li <?= ($post_object["ID"] == $current_id) ? ' class="submenu-active"' : ''; ?>>
                    <a href="<?php echo get_permalink($post_object["ID"]); ?>"><?php echo get_the_title($post_object["ID"]); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif;

?>