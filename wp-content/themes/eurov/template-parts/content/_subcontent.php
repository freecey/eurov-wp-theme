<?php

// Check rows exists.
if (have_rows('contenu_rep')) : ?>
    <section class="sub-content">

        <?php
        // Loop through rows.
        while (have_rows('contenu_rep')) : the_row();
            if (get_sub_field('separator') == 1) {
                echo do_shortcode('[SC_SEPARATOR]');
            };


            // Load sub field value.
            if (get_sub_field('nombre_de_colonne') == 1) { ?>
                <div class="my-4">
                    <div class="col-12 my-4">
                        <?= get_sub_field('contenu1'); ?>
                    </div>
                </div>
            <?php
            };

            if (get_sub_field('nombre_de_colonne') == 2) { ?>
                <div class="my-4 row">
                    <div class="col-12 col-sm-12 col-md-6">
                        <?= get_sub_field('contenu1'); ?>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6">
                        <?= get_sub_field('contenu2'); ?>
                    </div>
                </div>
            <?php
            };

            if (get_sub_field('nombre_de_colonne') == 3) { ?>
                <div class="my-4 row">
                    <div class="col-12 col-sm-12 col-md-4">
                        <?= get_sub_field('contenu1'); ?>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4">
                        <?= get_sub_field('contenu2'); ?>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4">
                        <?= get_sub_field('contenu3'); ?>
                    </div>
                </div>
        <?php
            };



        // Do something...

        // End loop.
        endwhile;

        // No value.
        // else :        
        // Do something...
        ?>
    </section>
<?php
endif;

?>