<?php
$show_address = get_field('show_address');
$show_gmap = get_field('show_gmap');
// args
$args = array(
    'post_type'      => 'info',
    'p' => 137
);

// query
$the_query = new WP_Query($args);

if ($the_query->have_posts()) :
    while ($the_query->have_posts()) : $the_query->the_post();
        if ($show_address == 1) :
            // echo 'SHOW ADDRESS';
            get_template_part('template-parts/content/_col2-content');
        endif;
        if ($show_gmap == 1) :
            // echo 'SHOW MAPS';
?>
            <div class="my-5"> </div>
<?php
            get_template_part('template-parts/content/_gmaps');
            get_template_part('template-parts/content/_col3-content');
        endif;
    endwhile;
endif;

// Restore original post data.
wp_reset_postdata();
