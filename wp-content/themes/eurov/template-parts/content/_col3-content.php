<?php

// Check rows exists.
if (have_rows('contenu_col3_rep')) : ?>
    <?php
    // Loop through rows.
    while (have_rows('contenu_col3_rep')) : the_row();
        if (get_sub_field('separator') == 1) {
            echo do_shortcode('[SC_SEPARATOR]');
        };


        // Load sub field value.
        if (get_sub_field('contenu-col3')) { ?>
            <div class="my-4">
                <?= get_sub_field('contenu-col3'); ?>
            </div>
<?php
        };

    // Do something...

    // End loop.
    endwhile;

// No value.
else :
// Do something...
endif;

?>