<?php
$images = get_field('galleries-list');
if ($images) :
?>
    <div class="card-logo">
        <?php foreach ($images as $image) :
            $add_space = array("-1", "-2", "-", "_"); ?>

            <!-- ADD ACTIVE ON FIRST -->
            <img src="<?php echo esc_url($image['url']); ?>" class="d-block my-2 mx-auto" alt="<?= ($image['alt'] != "") ?  esc_attr($image['alt']) : str_replace($add_space, " ", substr($image['filename'], 0, strpos($image['filename'], "."))); ?>" max-width="281">

        <?php
        endforeach; ?>
    </div>

<?php endif; ?>
</div>