<?php
$images = get_field('gallery');
if ($images) :
    $row_indicators = 0;
    $row_img = 0;
    $add_class_car = '';
    $car_type = get_field('carousel_type');
    if ($car_type == 'Portail') {
        $image_size = 'carousel-p';
    } elseif ($car_type == 'Portail-sans-rogner') {
        $image_size = 'carousel-p2';
        $add_class_car = ' carousel-p2';
    } else {
        $image_size = 'carousel';
    }
?>

    <!-- ADD INDICATOR  -->


    <div id="carouselIndicators" class="carousel slide carousel-fade" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <?php foreach ($images as $image) : ?>
                <button type="button" data-bs-target="#carouselIndicators" data-bs-slide-to="<?= $row_indicators; ?>" <?= ($row_indicators == 0) ? 'class="active" aria-current="true" ' : ''; ?>aria-label="Slide <?= $row_indicators; ?>" style="display: none;"></button>
            <?php $row_indicators++;
            endforeach; ?>
        </div>
        <div class="carousel-inner">
            <?php foreach ($images as $image) :
                $add_space = array("-1", "-2", "-", "_"); ?>
                <div class="carousel-item<?= ($row_img == 0) ? ' active' : ''; ?>">
                    <!-- ADD ACTIVE ON FIRST -->
                    <img src="<?php echo esc_url($image['sizes'][$image_size]); ?>" class="d-block w-100 <?= $add_class_car; ?>" alt="<?= ($image['alt'] != "") ?  esc_attr($image['alt']) : str_replace($add_space, " ", substr($image['filename'], 0, strpos($image['filename'], "."))); ?>">
                </div>
            <?php $row_img++;
            endforeach; ?>

        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>


    <?php endif; ?>


    </div>