<?php
function change_link($post_link, $id = 0)
{
    $post = get_post($id);
    if ($post->post_type == 'portfolio-maison') {
        if (is_object($post)) {
            $terms = wp_get_object_terms($post->ID, array('category-maison'));
            if ($terms) {
                return str_replace('%cat%', $terms[0]->slug, $post_link);
            }
        }
    }
    if ($post->post_type == 'portfolio-prof') {
        if (is_object($post)) {
            $terms = wp_get_object_terms($post->ID, array('category-Professionnels'));
            if ($terms) {
                return str_replace('%cat%', $terms[0]->slug, $post_link);
            }
        }
    }
    return   $post_link;
}
add_filter('post_type_link', 'change_link', 1, 3);

//load the template on the new generated URL otherwise you will get 404's the page
function generated_rewrite_rules()
{
    add_rewrite_rule(
        '^portfolio-maison/(.*)/(.*)/?$',
        'index.php?post_type=portfolio-maison&name=$matches[2]',
        'top'
    );
    add_rewrite_rule(
        '^portfolio-professionnels/(.*)/(.*)/?$',
        'index.php?post_type=portfolio-prof&name=$matches[2]',
        'top'
    );
}
add_action('init', 'generated_rewrite_rules');
