<?php
// MODIFY NAVBAR START 
function add_additional_class_on_li($classes, $item, $args)
{
    if (isset($args->add_li_class)) {
        // $classes = [];
        $classes[] = $args->add_li_class;
    }
    return $classes;
}

function add_menu_link_class($atts, $item, $args)
{
    if (property_exists($args, 'link_class')) {
        $atts['class'] = $args->link_class;
    }
    return $atts;
}



function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active ';
    }
    return $classes;
}

add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);
add_filter('nav_menu_css_class', 'special_nav_class', 1, 3);
add_filter('nav_menu_link_attributes', 'add_menu_link_class', 1, 3);

// MODIFY NAVBAR END 
