<?php

function add_separator()
{
    return '<div class="separator"></div>';
}

add_shortcode('SC_SEPARATOR', 'add_separator');
