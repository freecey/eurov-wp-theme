<?php

/**
 * Custom Post definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage EuroV
 * @since EuroV 1.0
 */

if (!defined('ABSPATH')) {
    die('-1');
}
function eurov_init2()
{
    // LINGE DE MAISON
    register_post_type('portfolio-maison', [
        'label' => 'Portfolio maison',
        'public' => true,
        'menu_position' => 11,
        'menu_icon' => 'dashicons-screenoptions',
        'supports' => ['title', 'thumbnail'],
        'has_archive' => false,
        // 'rewrite'     => array('slug' => 'portfolio-maison'),
        'rewrite' => array('slug' => 'portfolio-maison/%cat%'),
    ]);
    register_taxonomy('category-maison', 'portfolio-maison', [
        'labels' => [
            'name' => 'Categories Maison',
            'singular_name' => 'Catégorie Maison',
            'search_items' => 'Rechercher Catégories Maison',
            'all_items' => 'Tout Maison',
            'edit_item' => 'Éditer Catégorie Maison',
            'view_item' => 'Voir Catégorie Maison',
            'update_item' => 'Mettre à jour Catégorie Maison',
            'add_new_item' => 'Ajouter une nouvelle Catégorie Maison',
        ],
        'show_in_rest' => false,
        'hierarchical' => true,
        'show_admin_column' => true,
        'public' => true,
        'has_archive' => false,
    ]);
    // VÊTEMENTS PROFESSIONNELS
    register_post_type('portfolio-prof', [
        'label' => 'Portfolio Professionnels',
        'public' => true,
        'menu_position' => 12,
        'menu_icon' => 'dashicons-screenoptions',
        'supports' => ['title', 'thumbnail'],
        'has_archive' => false,
        'rewrite'     => array('slug' => 'portfolio-professionnels/%cat%'),
    ]);
    register_taxonomy('category-Professionnels', 'portfolio-prof', [
        'labels' => [
            'name' => 'Professionnels Categories',
            'singular_name' => 'Catégorie Professionnels',
            'search_items' => 'Rechercher Catégories Professionnels',
            'all_items' => 'Tout Professionnels',
            'edit_item' => 'Éditer Catégorie Professionnels',
            'view_item' => 'Voir Catégorie Professionnels',
            'update_item' => 'Mettre à jour Catégorie Professionnels ',
            'add_new_item' => 'Ajouter une nouvelle Catégorie Professionnels',
        ],
        'show_in_rest' => false,
        'hierarchical' => true,
        'show_admin_column' => true,
        'public' => true,
        'has_archive' => false,
    ]);
}

add_action('init', 'eurov_init2');




/**
 * Use radio inputs instead of checkboxes for term checklists in specified taxonomies.
 *
 * @param   array   $args
 * @return  array
 */
function wp_term_radio_checklist($args)
{
    if (!empty($args['taxonomy']) && ($args['taxonomy'] === 'category-Professionnels' or $args['taxonomy'] === 'category-maison') /* <== Change to your required taxonomy */) {
        if (empty($args['walker']) || is_a($args['walker'], 'Walker')) { // Don't override 3rd party walkers.
            if (!class_exists('WP_Walker_Category_Radio_Checklist')) {
                /**
                 * Custom walker for switching checkbox inputs to radio.
                 *
                 * @see Walker_Category_Checklist
                 */
                class WP_Walker_Category_Radio_Checklist extends Walker_Category_Checklist
                {
                    function walk($elements, $max_depth, ...$args)
                    {
                        $output = parent::walk($elements, $max_depth, ...$args);
                        $output = str_replace(
                            array('type="checkbox"', "type='checkbox'"),
                            array('type="radio"', "type='radio'"),
                            $output
                        );

                        return $output;
                    }
                }
            }

            $args['walker'] = new WP_Walker_Category_Radio_Checklist;
        }
    }

    return $args;
}

add_filter('wp_terms_checklist_args', 'wp_term_radio_checklist');
