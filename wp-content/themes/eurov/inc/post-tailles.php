<?php

/**
 * Custom Post definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage EuroV
 * @since EuroV 1.0
 */

if (!defined('ABSPATH')) {
    die('-1');
}
function eurov_init4()
{
    register_post_type('tailles', [
        'label' => 'Guide des tailles',
        'public' => true,
        'menu_position' => 14,
        'menu_icon' => 'dashicons-image-filter',
        'supports' => ['title', 'thumbnail'],
        //'show_in_rest' => true,
        'has_archive' => false,
    ]);
    // register_taxonomy('category-tailles', 'tailles', [
    //     'labels' => [
    //         'name' => 'tailles Categories',
    //         'singular_name' => 'Catégorie tailles',
    //         'search_items' => 'Rechercher Catégories tailles',
    //         'all_items' => 'Tout tailles',
    //         'edit_item' => 'Éditer Catégorie tailles',
    //         'view_item' => 'Voir Catégorie tailles',
    //         'update_item' => 'Mettre à jour Catégorie tailles ',
    //         'add_new_item' => 'Ajouter une nouvelle Catégorie tailles',
    //     ],
    //     'show_in_rest' => false,
    //     'hierarchical' => true,
    //     'show_admin_column' => true,
    //     'public' => true,
    //     'has_archive' => false,
    // ]);
}

add_action('init', 'eurov_init4');
