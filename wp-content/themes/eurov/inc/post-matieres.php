<?php

/**
 * Custom Post definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage EuroV
 * @since EuroV 1.0
 */

if (!defined('ABSPATH')) {
    die('-1');
}
function eurov_init3()
{
    register_post_type('matieres', [
        'label' => 'Les matières',
        'public' => true,
        'menu_position' => 13,
        'menu_icon' => 'dashicons-image-filter',
        'supports' => ['title', 'thumbnail'],
        //'show_in_rest' => true,
        'has_archive' => false,
    ]);
    register_taxonomy('category-matieres', 'matieres', [
        'labels' => [
            'name' => 'Matières Categories',
            'singular_name' => 'Catégorie Matières',
            'search_items' => 'Rechercher Catégories Matières',
            'all_items' => 'Tout Matières',
            'edit_item' => 'Éditer Catégorie Matières',
            'view_item' => 'Voir Catégorie Matières',
            'update_item' => 'Mettre à jour Catégorie Matières ',
            'add_new_item' => 'Ajouter une nouvelle Catégorie Matières',
        ],
        'show_in_rest' => false,
        'hierarchical' => true,
        'show_admin_column' => true,
        'public' => true,
        'has_archive' => false,
    ]);
}

add_action('init', 'eurov_init3');
