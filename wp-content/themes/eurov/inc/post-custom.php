<?php

/**
 * Custom Post definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage EuroV
 * @since EuroV 1.0
 */

if (!defined('ABSPATH')) {
    die('-1');
}
function eurov_init()
{
    register_post_type('info', [
        'label' => 'Accueil - Info',
        'public' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-store',
        'supports' => ['title', 'thumbnail'],
        //'show_in_rest' => true,
        'has_archive' => false,
    ]);
    // LINGE DE MAISON
    register_post_type('maison', [
        'label' => 'Linge de maison',
        'public' => true,
        'menu_position' => 6,
        'menu_icon' => 'dashicons-admin-multisite',
        'supports' => ['title', 'thumbnail'],
        'has_archive' => false,
        'rewrite'     => array('slug' => 'linge-de-maison'),
    ]);
    // register_taxonomy('category-maison', 'maison', [
    //     'labels' => [
    //         'name' => 'Categories Maison',
    //         'singular_name' => 'Catégorie Maison',
    //         'search_items' => 'Rechercher Catégories Maison',
    //         'all_items' => 'Tout Maison',
    //         'edit_item' => 'Éditer Catégorie Maison',
    //         'view_item' => 'Voir Catégorie Maison',
    //         'update_item' => 'Mettre à jour Catégorie Maison',
    //         'add_new_item' => 'Ajouter une nouvelle Catégorie Maison',
    //     ],
    //     'show_in_rest' => false,
    //     'hierarchical' => true,
    //     'show_admin_column' => true,
    //     'public' => true,
    //     'has_archive' => false,
    // ]);
    // VÊTEMENTS PROFESSIONNELS
    register_post_type('professionnels', [
        'label' => 'Vêtements Professionnels',
        'public' => true,
        'menu_position' => 7,
        'menu_icon' => 'dashicons-building',
        'supports' => ['title', 'thumbnail'],
        'has_archive' => false,
        'rewrite'     => array('slug' => 'vetements-professionnels'),
    ]);
    // register_taxonomy('category-Professionnels', 'professionnels', [
    //     'labels' => [
    //         'name' => 'Professionnels Categories',
    //         'singular_name' => 'Catégorie Professionnels',
    //         'search_items' => 'Rechercher Catégories Professionnels',
    //         'all_items' => 'Tout Professionnels',
    //         'edit_item' => 'Éditer Catégorie Professionnels',
    //         'view_item' => 'Voir Catégorie Professionnels',
    //         'update_item' => 'Mettre à jour Catégorie Professionnels ',
    //         'add_new_item' => 'Ajouter une nouvelle Catégorie Professionnels',
    //     ],
    //     'show_in_rest' => false,
    //     'hierarchical' => true,
    //     'show_admin_column' => true,
    //     'public' => true,
    //     'has_archive' => false,
    // ]);
    // ACCUEIL

}

add_action('init', 'eurov_init');
