<?php
// Usage:
// get_id_by_slug('any-page-slug');

function get_id_by_slug($page_slug)
{
    $page = get_page_by_path($page_slug);
    if ($page) {
        return $page->ID;
    } else {
        return null;
    }
}


function get_post_types_by_cust_taxonomy($tax)
{
    global $wp_taxonomies;
    return (isset($wp_taxonomies[$tax])) ? $wp_taxonomies[$tax]->object_type : array();
}



/**
 *  Function to get post ID By slug 
 */

function get_post_id_by_slug($slug)
{

    $post = get_page_by_path($slug);

    if ($post) {
        return $page->ID;
    } else {
        return null;
    }
}
